<?php

class BinaryUtils {

	public static $doors = ['AND', 'OR', 'NAND', 'NOR', 'XOR', 'XNOR', 'A1', 'A0'];

    public static function getRandomDoor() {
        return BinaryUtils::$doors[rand(0,sizeof(BinaryUtils::$doors)-1)];
    }

    public static function operate($i1, $i2, $operator)
    {
    	if($operator == "AND")
    		return $i1 and $i2;
    	else if($operator == "OR")
    		return $i1 or $i2;
    	else if($operator == "NAND")
    		return !($i1 & $i2);
    	else if($operator == "NOR")
    		return !($i1 | $i2);
    	else if($operator == "XOR")
    		return ($i1 xor $i2);
    	else if($operator == "XNOR")
    		return !($i1 xor $i2);
        else if($operator == "A1")
            return true;
        else if($operator == "A0")
            return false;
    	else 
    		die("unrecognized door : ".$operator);
    }
}

?>