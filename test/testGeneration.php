<?php

require_once('../config.inc.php');
require_once('../model/Player.php');
require_once('../model/Generation.php');
require_once('../model/BinaryUtils.php');
require_once('../model/PlayerFactory.php');
require_once('../model/DatabaseUtils.php');
require_once('../config.inc.php');

$time_start = microtime(true); 
/* generation_index, population_size = 0, input_size = 0, mutation_rate = 0.01, selection_rate */
$gen = new Generation(0, 1000, 4, 0.05, 0.5);
for ($i=0; $i < 100000; $i++) { 
	echo "generation ".$i." is alive.".PHP_EOL;
	$gen->live([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], [1,2,3,2,5,3,7,2,3,5,11,3,13,7,5]);
	if($i%100 == 0){
		echo "saving generation ".$i.".".PHP_EOL;
		DatabaseUtils::saveGeneration($gen);		
	}
	echo "generation ".$i." avg_hamming=".$gen->average_hamming." best_hamming=".$gen->best_hamming.PHP_EOL;
	$gen = $gen->mutate();
}
$time_end = microtime(true);
$execution_time = ($time_end - $time_start);
echo 'Total Execution Time: '.$execution_time.' seconds';
?>