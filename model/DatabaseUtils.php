<?php

require_once __DIR__ .'/../vendor/autoload.php';
use Medoo\Medoo;

class DatabaseUtils {

	private static $server = "localhost";
	private static $username = "root";
	private static $password = "";
	private static $db_name = "neural";

	public static function getDatabase(){
		return new Medoo([
		    'database_type' => 'mysql',
		    'database_name' => DatabaseUtils::$db_name,
		    'server' => DatabaseUtils::$server,
		    'username' => DatabaseUtils::$username,
		    'password' => DatabaseUtils::$password
		]);
	}

	public static function saveGeneration($generation)
	{	
		$database = DatabaseUtils::getDatabase();
		$database->insert('generation', [
		    'name' => $generation->name,
		    'average_hamming' => $generation->average_hamming,
		    'best_hamming' => $generation->best_hamming,
		    'data' => json_encode($generation)
		]);
		echo "generation id ".$database->id()." saved.".PHP_EOL;
	}

	public static function listGeneration()
	{
		$database = DatabaseUtils::getDatabase();
		return $database->select('generation', ['id', 'name', 'best_hamming', 'average_hamming']);
	}
}

?>