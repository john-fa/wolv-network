<?php

require_once('Player.php');
require_once('BinaryUtils.php');

class PlayerFactory {

	public static function createPlayer($inputs) {
		$player = new Player();
        $player->name = PlayerFactory::generateName();
    	$player->inputs = $inputs;
    	$player->nb_comb = PlayerFactory::combinations($player->getInputSize(),2);
    	$player->nb_pc = PlayerFactory::pfactorial($player->getInputSize()-1);


    	/* Deep network with random connections */
    	for ($i=0; $i <= $player->nb_comb*$player->nb_comb; $i++) {
    		array_push($player->adn, BinaryUtils::getRandomDoor());
    	}
    	/* Exit of the network */
    	for ($i=0; $i <= ($player->nb_comb-$player->getInputSize()); $i++) {
            for ($j=0; $j <= ($player->nb_comb-$i); $j++) {
                array_push($player->adn, BinaryUtils::getRandomDoor());
            }
    	}

    	/* Initial connexions */
    	$adn_index = 0;
    	for ($i=0; $i < $player->getInputSize(); $i++) {
    		for ($j=$i+1; $j < $player->getInputSize(); $j++) {
    			$player->addConnexion("i".$i, "i".$j, $adn_index++);
    		}
    	}

    	/*Deep network connexion */
    	for ($i=0; $i < $player->nb_comb-1; $i++) {
            $possibilities = array();
            for ($j=$i*$player->nb_comb; $j < ($i*$player->nb_comb)+$player->nb_comb; $j++) {
                $possibilities[] = $j;
            }
            $original_possibilities = $possibilities;
            while(sizeof(array_intersect_assoc($possibilities, $original_possibilities)) > 0)
                shuffle($possibilities);
            for ($j=0; $j < $player->nb_comb; $j++) {
                $player->addConnexion(($i*$player->nb_comb)+$j, $possibilities[$j], ($i*$player->nb_comb)+$j+$player->nb_comb);        		
            }
    	}

        /* Closing connexion */
        $deep_end = $player->nb_comb*($player->nb_comb-1);
        $nb_columns = $player->nb_comb-$player->getInputSize();
        for ($i=1; $i <= $nb_columns ; $i++) {
            $column_size =  $player->nb_comb-$i;
            for ($j=0; $j < $column_size; $j++) { 
                $player->addConnexion($deep_end+$j, $deep_end+$j+1, $deep_end+$column_size+$j+1);
            }
            $deep_end = $deep_end+$column_size+1;
        }

    	return $player;
    }

    public static function factorial($n) 
	{
		if ($n == 0)
		{
			return 1;
		}
		else
		{
			return $n * PlayerFactory::factorial($n - 1);
		}
	}

	public static function pfactorial($n) 
	{
		if ($n == 0)
		{
			return 1;
		}
		else
		{
			return $n + PlayerFactory::pfactorial($n - 1);
		}
	}

	public static function combinations($n, $r)
	{
		return PlayerFactory::factorial($n)/(PlayerFactory::factorial($r)*(PlayerFactory::factorial($n-$r)));
	}

    public static function generateName()
    {
        $names = array(
            'Christopher','Ryan','Ethan','John',
            'Zoey','Sarah','Michelle','Samantha',
            'Pidor','Bunthoeun','Peter','Bruce',
            'Clark','Jessica','Thomas','Angel'
        );
        $surnames = array(
            'Walker','Thompson','Anderson','Johnson',
            'Tremblay','Peltier','Cunningham','Bolt',
            'Simpson','Mercado','Sellers','Robson', 
            'Parker','Wayne','Kent','Jones','Chhiv'
        );
        $middle_name = substr('ABCDEFGHIJKLMNOPQRSTUVWXYZ',rand(0,25),1);
        $random_name = $names[mt_rand(0, sizeof($names) - 1)];
        $random_surname = $surnames[mt_rand(0, sizeof($surnames) - 1)];
        return $random_name . ' ' . $middle_name . '. ' . $random_surname;
    }

}

?>