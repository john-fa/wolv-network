<?php

require_once('../config.inc.php');
require_once('../model/Player.php');
require_once('../model/BinaryUtils.php');
require_once('../model/PlayerFactory.php');

header('Content-Type: application/json');
$player = PlayerFactory::createPlayer(["i1","i2","i3","i4"]);
$return = $player->toSigmaNetwork();
$return['nb_comb'] = $player->nb_comb;
$return['input_size'] = $player->getInputSize();
$return['play6'] = $player->play(6);
$return['play7'] = $player->play(7);
$return['play8'] = $player->play(8);
$return['play9'] = $player->play(9);
$return['play10'] = $player->play(10);
$return['play11'] = $player->play(11);
$return['play12'] = $player->play(12);
$return['play13'] = $player->play(13);
$return['play14'] = $player->play(14);
$return['play15'] = $player->play(15);
$return['play16'] = $player->play(16);
echo json_encode($return);
die();

?>