<?php

require_once('BinaryUtils.php');

class Player {

	var $adn = array();

    var $name;

    var $connexions = array();

    var $inputs = array();

    var $memory = array();

    var $result = array('total_hamming' => 0);

    var $nb_comb;

    var $nb_pc;

    var $nb_mutations = 0;

    var $nb_initial_connexions = 0;

    var $log = array();

    public function reborn(){
        $this->result = array('total_hamming' => 0);
    }

    public function getClosingConnexions()
    {
        $nb = 0;
        for ($i=$this->nb_comb; $i > $this->getInputSize(); $i--) { 
           $nb += $i-1;
        }
        return $nb;
    }
    public function getInitialConnexions()
    {
        return $this->nb_initial_connexions;
    }

    public function mutate($mutation_rate)
    {
        $instance = new self();
        $instance->nb_pc = $this->nb_pc;
        $instance->nb_comb = $this->nb_comb;
        $instance->nb_mutations = $this->nb_mutations;
        $instance->inputs = $this->inputs;
        $instance->connexions = array_fill (0,sizeof($this->connexions),[-1,-1,-1]);
        $instance->connexions = $this->connexions;

        // Mutate DNA  
        foreach ($this->adn as $i => $strand) {
            if(rand(0,100) < 100*$mutation_rate)
            {
                $instance->adn[$i] = BinaryUtils::getRandomDoor();
                $instance->log[] = " - mutating DNA ".$i." (".$strand." => ".$instance->adn[$i].")";
                $instance->nb_mutations++;
            }
            else
            {
                $instance->adn[$i] = $strand;
            }
        }

        // Mutate links between DNA strands
        $unmutable = array();
        $nb_closing_connexions = $this->getClosingConnexions();
        $instance->log[] = "closing connexions : ".$nb_closing_connexions;
        for ($i=0; $i < sizeof($this->connexions); $i++) { 
            $connexion = $instance->connexions[$i];
            if(rand(0,100) < 100*$mutation_rate && !(strpos($connexion[0], 'i') !== false) && !in_array($i, $unmutable) && $i < sizeof($this->connexions)-$nb_closing_connexions) {
                $instance->log[] = " - mutating connexion ".$i." (".$connexion[0].",".$connexion[1].",".$connexion[2].")";
                $possibilities = array();
                $source_start_interval = floor($connexion[0]/$this->nb_comb)*$this->nb_comb;
                $source_end_interval = $source_start_interval + $this->nb_comb;
                $instance->log[sizeof($instance->log)-1] .= " [srcStart:".$source_start_interval.'-scrEnd:'.$source_end_interval."] ";
                for ($j=$source_start_interval; $j < $source_end_interval; $j++) { 
                    if($j != $connexion[0] && $j != $connexion[1])
                        $possibilities[] = $j;
                }
                $new_connector = $possibilities[rand(0,sizeof($possibilities)-1)];
                if(rand(0,100) > 50) // replace top connexion
                {
                    $old_connector = $connexion[0];
                    $connexion[0] = $new_connector;                    
                }
                else // replace bottom connexion
                {
                    $old_connector = $connexion[1];
                    $connexion[1] = $new_connector;
                }
                $instance->log[sizeof($instance->log)-1] .= " => (".$connexion[0].",".$connexion[1].",".$connexion[2].")";
                $compensation = -1;
                for ($j=$this->nb_comb; $j < sizeof($instance->connexions) ; $j++) { // replace connexion in another node
                    if(!in_array($j, $unmutable))
                    {
                        if($instance->connexions[$j][0] == $new_connector && $instance->connexions[$j][1] != $old_connector) {
                            //$instance->connexions[$j] = $this->connexions[$j];
                            $compensation = $j;
                            $instance->connexions[$j][0] = $old_connector;
                            $instance->connexions[$i] = $connexion; 
                            $instance->log[sizeof($instance->log)-1] .= " + (".$instance->connexions[$j][0].",".$instance->connexions[$j][1].",".$instance->connexions[$j][2].")";
                            break;
                        }
                        else if ($instance->connexions[$j][1] == $new_connector && $instance->connexions[$j][0] != $old_connector) {
                            //$instance->connexions[$j] = $this->connexions[$j];
                            $compensation = $j;
                            $instance->connexions[$j][1] = $old_connector; 
                            $instance->connexions[$i] = $connexion;
                            $instance->log[sizeof($instance->log)-1] .= " + (".$instance->connexions[$j][0].",".$instance->connexions[$j][1].",".$instance->connexions[$j][2].")"; 
                            break;
                        }
                    }
                }
                if($compensation == -1)
                    $instance->log[sizeof($instance->log)-1] .= " aborted";
                else
                    $instance->log[sizeof($instance->log)-1] .= " <".$i.",".$j.">";
                if($j > $i) {
                    $unmutable[] = $j;
                }                
            }
            else if(!in_array($i, $unmutable))
            {           
                $instance->connexions[$i] = $connexion;
            }
        }
        $instance->log[] = $unmutable;
        $instance->name = $this->name;
        $instance->inputs = $this->inputs;
        return $instance;
    }

    public function getTotalHamming()
    {
        return $this->result['total_hamming'];
    }

    public function checkNetworkCompliance()
    {
        $links_count = array();
        for ($i=0; $i < sizeof($this->connexions); $i++) { 
            if(!isset($links_count[$this->connexions[$i][0]]))
                $links_count[$this->connexions[$i][0]] = ['in' => 0, 'out' => 0];
            $links_count[$this->connexions[$i][0]]['in']++;
            if(!isset($links_count[$this->connexions[$i][1]]))
                $links_count[$this->connexions[$i][1]] = ['in' => 0, 'out' => 0];
            $links_count[$this->connexions[$i][1]]['in']++;
            if(!isset($links_count[$this->connexions[$i][2]]))
                $links_count[$this->connexions[$i][2]] = ['in' => 0, 'out' => 0];
            $links_count[$this->connexions[$i][2]]['out']++;
        }
        for ($i=0; $i < 30; $i++) { 
            if($links_count[$i]['in'] != 2)
                return "problem with ".$i;
        }
        return $links_count;
    }

    public function play($input)
    {
        $bin = decbin($input);
        if(strlen($bin) > $this->getInputSize())
        {
            return "input is too large";
        }
        else if(strlen($bin) < $this->getInputSize())
        {
            $bin=str_pad($bin, $this->getInputSize(), "0", STR_PAD_LEFT);
        }
        for ($i = 0; $i < strlen($bin); $i++){
            $memory["i".$i] = intval($bin[$i]);
        }
        foreach ($this->connexions as $index => $value) {
          $memory[$value[2]] = BinaryUtils::operate($memory[$value[0]], $memory[$value[1]], $this->adn[$index]);  
        }
        $output = array_slice($memory, sizeof($memory)-4, sizeof($memory));
        $str_output = "";
        foreach ($output as $value) {
            if($value)
                $str_output .= "1";
            else
                $str_output .= "0";
        }
        $output = bindec($str_output);
        $this->result[$input] = [
            'input' => $input,
            'output' => $output,
            'hamming' => gmp_hamdist($input, $output)
        ];
        $this->result['total_hamming'] += $this->result[$input]['hamming'];
        return $output;
    }

    public function printPlayer() {
    	Player::simplePrintArray($this->adn, "ADN");
    	Player::simplePrintArray($this->inputs, "INPUT");
    	Player::simplePrintMatrix($this->connexions, "CONNEXIONS");
    	Player::simplePrintMatrix($this->toMatrix(), "NETWORK");
    }

    public function getInputSize()
    {
    	return sizeof($this->inputs);
    }

    public function addConnexion($i1, $i2, $adnIndex)
    {
    	array_push($this->connexions, array($i1, $i2, $adnIndex));
    }

	public function toMatrix()
	{
		$matrix = array();
		array_push($matrix, $this->inputs);
		for ($i=0; $i < $this->nb_comb; $i++) {
			array_push($matrix, array_slice($this->adn, $i*$this->nb_comb, $this->nb_comb));
		}
        for ($i=1; $i <= $this->nb_comb-$this->getInputSize(); $i++) {
            array_push($matrix, array_slice($this->adn, $i*$this->nb_comb, $this->nb_comb-$i));
        }
		return $matrix;
	}

    public function toSigmaNetwork()
    {
        $return = array(
            "compliance" => $this->checkNetworkCompliance(),
            "nodes" => [],
            "edges" => [],
            "log" => $this->log,
            "player" => $this
        );
        $matrix = $this->toMatrix();
        $cpt = 0;
        foreach ($matrix as $index => $line) {
            if($index == 1) $cpt = 0;
            if($index == 0) $c = 10; else $c = 0;
            if($index == 0) $prefix = "i"; 
            else $prefix = "";
            foreach ($line as $indexl => $node) 
            {
                $return['nodes'][] = array(
                    "id" => $prefix.$cpt++,
                    "x" => $index*10,
                    "y" => $indexl*10+$c,
                    "label" => $node." ".($cpt-1),
                    "size" => 2
                );
            }
        }
        foreach ($this->connexions as $index => $connexion) 
        {
            $return['edges'][] = array(
                "id" => "e".$index."-1",
                "source" => $connexion[0],
                "target" => $connexion[2]
            );
            $return['edges'][] = array(
                "id" => "e".$index."-2",
                "source" => $connexion[1],
                "target" => $connexion[2]
            );
        }
        return $return;
    }

	public static function simplePrintArray($array, $title)
	{
		echo "<table><tr><td><b>".$title."</b></td>";
    	foreach ($array as $elem) {
    		echo "<td>".$elem."</td>";
    	}
    	echo "</tr></table>";
	}

	public static function simplePrintMatrix($matrix, $title)
	{
		echo $title;
		echo "<table>";
    	foreach ($matrix as $array) {
    		echo "<tr>";
    		foreach ($array as $elem) {
    			echo "<td>".$elem."</td>";
    		}
    		echo "</tr>";
    	}
    	echo "</table>";
	}

}

?>