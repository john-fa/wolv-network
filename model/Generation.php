<?php

require_once('BinaryUtils.php');
require_once('PlayerFactory.php');
class Generation {

	var $name;
	var $generation_index;
	var $generation_predecessor;

	/* array containing all the players */
	var $population = array();

	/* probability of mutation of ADN */
	var $mutation_rate = 0.01;

	/* % player surviving a mutation */ 
	var $selection_rate = 0.1;

	var $best_hamming = 999999;
	var $best_hamming_player = 0;
	var $average_hamming = 999999;

	function __construct($generation_index, $population_size = 0, $input_size = 0, $mutation_rate = 0.01, $selection_rate = 0.1) {
		if($population_size > 0 && $input_size > 0){
			$inputs = [];
			for ($i=0; $i < $input_size; $i++) { 
				$inputs[] = "i".$i;
			}
			for ($i=0; $i < $population_size; $i++) { 
				$this->population[] = PlayerFactory::createPlayer($inputs);
			}
		}
		$this->mutation_rate = $mutation_rate;
		$this->generation_index = $generation_index;
		$this->selection_rate = $selection_rate;
		$this->name = PlayerFactory::generateName();

	}

	function mutate()
	{
		$instance = new self($this->generation_index+1);
		$instance->mutation_rate = $this->mutation_rate;
		$instance->selection_rate = $this->selection_rate;
		$instance->best_hamming = 9999999;
		$instance->name = PlayerFactory::generateName();
		$nb_survivors = floor($this->selection_rate*sizeof($this->population));
		for ($i=0; $i < sizeof($this->population); $i++) { 
			if($i < $nb_survivors){
				$this->population[$i]->reborn();
				$instance->population[] = $this->population[$i];
			}
			else{
				$instance->population[] = $this->population[($i%$nb_survivors)]->mutate($this->mutation_rate);
			}
		}
		$instance->generation_predecessor = $this->name;
		return $instance;
	}

	function live($inputs, $expected_results)
	{
		if(sizeof($inputs) != sizeof($expected_results))
			die("inputs and expected results have different size.");
		for ($i=0; $i < sizeof($this->population); $i++) {
			foreach ($inputs as $index => $input) {
				$this->population[$i]->play($input);
			}
		}
		//krumo($this->population);
		usort($this->population, array('Generation','comparePlayer'));
		//krumo($this->population);
		$this->best_hamming = $this->population[0]->getTotalHamming();
		$sum = 0;
		for ($i=0; $i < sizeof($this->population); $i++) {
			$sum += $this->population[$i]->getTotalHamming();
			if($this->best_hamming == $this->population[$i]->getTotalHamming())
				$this->best_hamming_player++;
		}
		$this->average_hamming = $sum / sizeof($this->population);
	}

	function toJSON()
	{
		$return = 	[
						"generation_name" => $this->name,
						"best_hamming" => $this->best_hamming,
						"best_hamming_player" => $this->best_hamming_player,
						"average_hamming" => $this->average_hamming,
						"population" => $this->population
					];
		return $return;
	}

	/* to be used with usort */
	public static function comparePlayer($a, $b)
	{
	    return $a->getTotalHamming() > $b->getTotalHamming();
	}

}

?>