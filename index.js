var appMessage,appPlayerList;
var g,s;


$( document ).ready(function() {

	g = {
	    nodes: [],
	    edges: []
	};
	s = new sigma({
		graph: g,
		container: 'player-display',
		settings: {	defaultNodeColor: '#ec5148'	}
	});

	appMessage = new Vue({
		el: '#message',
		data: {
			seen: false,
			message: 'Error or function messages panel'
		}
	});

	appPlayerList = new Vue({
		el: '#players-list',
		data: {
			seen: true
		},
		methods: {
			createPlayer: function (event) {
				console.log("loading new player");
				s.graph.clear();				
				sigma.parsers.json("services/player/test.php", s, function() {
					s.refresh();
				});
			},
			clearGraph: function (event) {
				s.graph.clear();
				s.refresh();
			}
		}
	});
});

	
	