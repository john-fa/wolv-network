<HTML>
	<HEAD>
		<meta charset="utf-8">
		<link rel="stylesheet" href="node_modules/skeleton-css/css/normalize.css">
	  	<link rel="stylesheet" href="node_modules/skeleton-css/css/skeleton.css">
		<script src="node_modules/vue/dist/vue.js"></script>
		<script src="node_modules/jquery/dist/jquery.min.js"></script>
		<script src="node_modules/sigma/build/sigma.min.js"></script>
		<script src="node_modules/sigma/build/plugins/sigma.parsers.json.min.js"></script>
		<link rel="stylesheet" href="index.css">
	</HEAD>
	<BODY>
		<div class="container">
			<div id="generation-list" v-if="seen">
				<ul id="example-2">
					<li v-for="(gen, index) in generations">
						{{ gen.id }} - {{ gen.name }}
					</li>
				</ul>
			</div>
			<div id="players-list" v-if="seen">
				<p><a v-on:click="createPlayer" href='#'>Create a new player</a> | <a v-on:click="clearGraph" href='#'>Clear</a></p>
			</div>	
			<div id="player-display" v-if="seen">				
			</div>		
			<div v-if="seen" id="message">{{message}}</div>
		</div>
	</BODY>
	<script src="index.js?v=3"></script>
</HTML>